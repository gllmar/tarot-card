

```
Technical Specification
Overview

The project involves creating a kiosk site using HTML, CSS, and JavaScript that functions as a museum installation. The site plays videos based on RFID tags and MIDI Note On events. The site includes a hidden debug mode for testing and configuration.
Requirements

    HTML Structure:
        A standbyScreen for displaying a standby mode with a fade-in and fade-out text label.
        Two video elements (videoPlayerA and videoPlayerB) for playing videos with crossfade transitions.
        An overlay for displaying video information (videoInfo) with name and caption.
        A debug toggle button to show/hide the debug menu.
        A debug menu with various controls:
            Enable logging.
            Show video controls.
            Show info overlay.
            Enable MIDI input.
            Master volume control.
            MIDI device selection.

    CSS Styling:
        Make video players take all available pixels without modifying the original ratio.
        Style the debug menu to ensure each element and its label are on the same line.
        Make the debug menu responsive and mobile-friendly.
        Center the standby screen text label with a slow fade-in and fade-out animation.


    JavaScript Functionality:
        RFID Input Handling:
            Capture RFID input as fast bursts of keyboard input and trigger video playback based on the tag.
            Use a buffer to collect input characters until an 'Enter' key is detected.
        Video Playback:
            Ensure each RFID tag can be associated with multiple videos, played randomly without repetition.
            Implement crossfade transitions between videos, including audio crossfade.
            Ensure proper video loading before starting transitions.
            Return to standby mode when videos finish playing.
        Debug Mode:
            Provide on-screen buttons to simulate RFID input.
            Log all incoming keystrokes to an onscreen log.
            Show/hide debug menu with a toggle button, and make it mobile-friendly.
            Allow testing all possible tags in the JSON with a button in debug mode.
            Enable logging and video controls visibility via checkboxes.
            Display video information overlay with a toggle button.
            Provide master volume control in the debug menu.
        MIDI Input Handling:
            Enable MIDI input to trigger video playback using Note On events.
            Allow MIDI input enabling/disabling in debug mode.
            Provide a menu to select the MIDI device input.

    Database Structure:
        External JSON file (rfidDatabase.json) to store RFID tags and associated videos.
        JSON structure example:

        json

        {
          "1": {
            "keytag": ["11111"],
            "videos": ["media/lorem/ipsum_010.mp4", "media/lorem/ipsum_011.mp4", "media/lorem/ipsum_012.mp4", "media/lorem/ipsum_013.mp4", "media/lorem/ipsum_014.mp4"],
            "name": "Tag 11111",
            "caption": "This is the caption for Tag 11111",
            "NoteOnMidiMap": 32
          },
          ...
        }

        Ensure each entry has:
            A unique identifier.
            An array of associated keytags.
            An array of associated video paths with leading zeros for hundreds.
            Name and caption for display.
            A NoteOnMidiMap field for MIDI mapping, starting at 32 and incrementing by 1 for each entry.

    Error Handling:
        Check for video file existence before attempting to play it.
        Gracefully handle errors by logging and continuing to process the next video in the queue.

    Crossfade Management:
        Implement smooth crossfade transitions between videos without intermediate black frames.
        Ensure audio volume transitions smoothly along with video opacity.
        Prevent enterStandbyMode from triggering during crossfade transitions.

Implementation Details

    HTML Structure:
        Elements are structured with appropriate IDs and classes for styling and JavaScript interaction.

    CSS Styling:
        Flexbox is used to align labels and inputs on the same line in the debug menu.
        Elements are styled to ensure a clean and user-friendly interface.

    JavaScript Functionality:
        Initialization:
            Load RFID database from an external JSON file.
            Initialize Web Audio API for handling audio transitions.
            Setup event listeners for key inputs, video endings, and debug menu interactions.
        RFID Handling:
            Process RFID input to match tags in the database and queue videos for playback.
            Use getVideoForRFIDTag to fetch video details and processVideoQueue to manage video playback.
        Video Playback and Transitions:
            Ensure videos are preloaded before starting transitions.
            Manage crossfade transitions with loadAndCrossFadeVideos and fadeElement functions.
            Handle video end events to trigger standby mode, with checks to avoid interfering with crossfade transitions.
        Debug Mode:
            Provide functionalities for testing and configuring the kiosk site, including RFID simulation, logging, and toggling video controls and overlays.
        MIDI Handling:
            Enable and manage MIDI input for triggering video playback based on Note On events.
            Provide a user interface for selecting MIDI devices and enabling/disabling MIDI input.

These specifications cover the functionality and structure needed for the kiosk site, ensuring a robust and user-friendly experience for both users and developers in debug mode.