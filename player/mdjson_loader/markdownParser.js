export function parseMarkdown(markdown) {
    const lines = markdown.split('\n');
    const database = [];
    let currentEntry = null;

    lines.forEach(line => {
        line = line.trim();

        if (line.startsWith('## ')) {
            if (currentEntry) {
                database.push(currentEntry);
            }
            currentEntry = {};
        } else if (line.startsWith('### ')) {
            const key = line.substring(4).toLowerCase();
            currentEntry[key] = null;
        } else if (line.startsWith('* ')) {
            const value = line.substring(2).trim();

            if (currentEntry.videos === null) {
                currentEntry.videos = [];
            }

            if (currentEntry.id === null || currentEntry.label === null || currentEntry.caption === null || currentEntry.keytag === null || currentEntry.noteonmidimap === null) {
                const key = Object.keys(currentEntry).find(k => currentEntry[k] === null);
                currentEntry[key] = value;
            } else if (currentEntry.videos) {
                const match = value.match(/\[!\[(.*?)\]\((.*?)\)\]\((.*?)\)/);
                if (match) {
                    const [_, text, thumbnail, url] = match;
                    currentEntry.videos.push({ text, thumbnail, url });
                } else {
                    const fallbackMatch = value.match(/\[(.*?)\]\((.*?)\)/);
                    if (fallbackMatch) {
                        const [_, text, url] = fallbackMatch;
                        currentEntry.videos.push({ text, thumbnail: '', url });
                    } else {
                        currentEntry.videos.push({ text: value, thumbnail: '', url: value });
                    }
                }
            }
        }
    });

    if (currentEntry) {
        database.push(currentEntry);
    }

    return database;
}
