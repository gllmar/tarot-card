## database in markdown

[demo-player-parser.html](/player/mdjson_loader/index.html)

[database.md](./database.md)

[database with thumb](/player/mdjson_loader/media/lorem/database.md)


```
./database.md
```
is database for video player

Parsed at runtime with [markdownParser.js](./markdownParser.js)




## single video player logic 

### wait till video ended to trigger next video

```mermaid
stateDiagram
    [*] --> Standby
    Standby --> Loading: Start Playback
    Loading --> Playing: Video Loaded
    Playing --> CheckQueue: Video Ended
    CheckQueue --> LoadingNext: Queue Not Empty
    CheckQueue --> Standby: Queue Empty
    LoadingNext --> Playing: Next Video Loaded
    Playing --> Paused: Pause
    Paused --> Playing: Play
    Playing --> Standby: Stop
    Paused --> Standby: Stop
    LoadingNext --> Standby: Stop

    state Standby {
        [*] --> Idle
        Idle --> StartPlayback: Start Playback
    }

    state Loading {
        [*] --> LoadVideo
        LoadVideo --> VideoLoaded: Video Loaded
    }

    state Playing {
        [*] --> PlayVideo
        PlayVideo --> VideoEnded: Video Ended
        PlayVideo --> Pause: Pause
    }

    state CheckQueue {
        [*] --> QueueNotEmpty
        QueueNotEmpty --> LoadNextVideo: Load Next Video
        QueueNotEmpty --> QueueEmpty: Queue Empty
    }

    state LoadingNext {
        [*] --> LoadNextVideo
        LoadNextVideo --> NextVideoLoaded: Next Video Loaded
        LoadNextVideo --> Stop: Stop
    }

    state Paused {
        [*] --> PauseVideo
        PauseVideo --> Play: Play
        PauseVideo --> Stop: Stop
    }

```



### note on test media

```json
## 1
### id
* 1
### label 
* Tag 11111
### caption
* This is the caption for Tag 11111
### videos
* ![[ipsum_010](ipsum_010.mp4)](thumb_ipsum_010.webp)
* [ipsum_011](ipsum_011.mp4)
* [ipsum_012](ipsum_012.mp4)
* [ipsum_013](ipsum_013.mp4)
* [ipsum_014](ipsum_014.mp4)
### keytag
* 3755233229
### NoteOnMidiMap
* 36

```