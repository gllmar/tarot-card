import { parseMarkdown } from './markdownParser.js';

let queue = null;
let playedVideos = new Set();
let currentVideo = null;

document.getElementById('debugMenuToggle').addEventListener('click', () => {
    const debugMenu = document.getElementById('debugMenu');
    debugMenu.style.display = debugMenu.style.display === 'block' ? 'none' : 'block';
});

document.getElementById('toggleHierarchyButton').addEventListener('click', () => {
    toggleHierarchy();
});

document.getElementById('toggleControlsButton').addEventListener('click', () => {
    toggleVideoControls();
});

document.getElementById('toggleFullscreenButton').addEventListener('click', () => {
    toggleFullscreen();
});

const videoPlayer = document.getElementById('videoPlayer');
const captionElement = document.getElementById('caption');
const standbyElement = document.getElementById('standby');
const nowPlayingLabel = document.getElementById('nowPlayingLabel');
const nextVideoLabel = document.getElementById('nextVideoLabel');

videoPlayer.addEventListener('ended', () => {
    if (queue) {
        playNextInQueue();
    } else {
        enterStandbyMode();
    }
});

function playNextInQueue() {
    const { url, caption } = queue;
    queue = null;
    updateNextVideoLabel();
    playVideo(url, caption);
}

function playVideo(url, caption) {
    currentVideo = url;
    videoPlayer.src = url;
    videoPlayer.load();
    videoPlayer.onloadeddata = () => {
        videoPlayer.play();
        captionElement.textContent = caption;
        nowPlayingLabel.textContent = `▶️ ${url}`;
        exitStandbyMode();
    };
}

function getNextRandomVideo(videos) {
    if (videos.length === 1) return videos[0];

    let filteredVideos = videos.filter(video => video.url !== currentVideo && !playedVideos.has(video.url));
    if (filteredVideos.length === 0) {
        playedVideos.clear();
        filteredVideos = videos.filter(video => video.url !== currentVideo);
    }
    
    const randomIndex = Math.floor(Math.random() * filteredVideos.length);
    return filteredVideos[randomIndex];
}

function playRandomVideo(videos, caption) {
    const nextVideo = getNextRandomVideo(videos);
    queue = { url: nextVideo.url, caption: caption };
    updateNextVideoLabel();
    if (videoPlayer.paused || videoPlayer.ended) {
        playNextInQueue();
    }
}

function updateNextVideoLabel() {
    nextVideoLabel.textContent = queue ? `⏭️ ${queue.url}` : '⏭️ None';
}

function toggleVideos(event) {
    const headerDiv = event.target.closest('.header');
    const videosDiv = headerDiv.nextElementSibling;
    const isVisible = videosDiv.style.display === 'block';
    videosDiv.style.display = isVisible ? 'none' : 'block';
    event.target.textContent = isVisible ? '▶' : '▼';
}

function generateInteractiveHierarchy(database) {
    const interactiveHierarchy = document.getElementById('interactiveHierarchy');
    interactiveHierarchy.innerHTML = '';
    database.forEach(entry => {
        const entryDiv = document.createElement('div');
        
        const headerDiv = document.createElement('div');
        headerDiv.className = 'header';
        
        const toggleButton = document.createElement('button');
        toggleButton.className = 'toggle';
        toggleButton.textContent = '▶';
        toggleButton.addEventListener('click', toggleVideos);
        headerDiv.appendChild(toggleButton);
        
        const idSpan = document.createElement('span');
        idSpan.textContent = `ID: ${entry.id}`;
        idSpan.className = 'id';
        idSpan.addEventListener('click', () => playRandomVideo(entry.videos, entry.caption));
        headerDiv.appendChild(idSpan);
        
        entryDiv.appendChild(headerDiv);
        
        const videosDiv = document.createElement('div');
        videosDiv.className = 'videos';
        entry.videos.forEach(video => {
            const videoDiv = document.createElement('div');
            videoDiv.className = 'video-entry';
            videoDiv.addEventListener('click', () => {
                queue = { url: video.url, caption: entry.caption };
                updateNextVideoLabel();
                if (videoPlayer.paused || videoPlayer.ended) {
                    playNextInQueue();
                }
            });

            if (video.thumbnail) {
                const thumbnailImg = document.createElement('img');
                thumbnailImg.src = video.thumbnail;
                thumbnailImg.alt = video.text;
                thumbnailImg.className = 'thumbnail';
                videoDiv.appendChild(thumbnailImg);
            }

            const videoSpan = document.createElement('span');
            videoSpan.textContent = video.text;
            videoDiv.appendChild(videoSpan);

            videosDiv.appendChild(videoDiv);
        });
        entryDiv.appendChild(videosDiv);
        
        interactiveHierarchy.appendChild(entryDiv);
    });
}

function toggleHierarchy() {
    const interactiveHierarchy = document.getElementById('interactiveHierarchy');
    if (interactiveHierarchy.style.display === 'none') {
        interactiveHierarchy.style.display = 'block';
    } else {
        interactiveHierarchy.style.display = 'none';
    }
}

function toggleVideoControls() {
    if (videoPlayer.hasAttribute('controls')) {
        videoPlayer.removeAttribute('controls');
        document.getElementById('toggleControlsButton').textContent = '🎛️';
    } else {
        videoPlayer.setAttribute('controls', 'controls');
        document.getElementById('toggleControlsButton').textContent = '🎛️';
    }
}

function toggleFullscreen() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
        document.getElementById('toggleFullscreenButton').textContent = '🖥️';
    } else if (document.exitFullscreen) {
        document.exitFullscreen();
        document.getElementById('toggleFullscreenButton').textContent = '🖥️';
    }
}

function enterStandbyMode() {
    videoPlayer.style.display = 'none';
    standbyElement.style.display = 'flex';
    nowPlayingLabel.textContent = '▶️ None';
}

function exitStandbyMode() {
    standbyElement.style.display = 'none';
    videoPlayer.style.display = 'block';
}

function initializePlayer(database) {
    generateInteractiveHierarchy(database);
    updateNextVideoLabel();
}

// Fetch and parse the markdown file
fetch('database.md')
    .then(response => response.text())
    .then(markdown => {
        const database = parseMarkdown(markdown);
        initializePlayer(database);
    })
    .catch(error => console.error('Error loading the markdown file:', error));
