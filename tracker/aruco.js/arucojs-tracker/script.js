let videoElement = document.getElementById('videoElement');
let canvas = document.getElementById('canvas');
let cameraSelect = document.getElementById('cameraSelect');
let zoomControl = document.getElementById('zoomControl');
let detectionToggle = document.getElementById('detectionToggle');
let showIdsToggle = document.getElementById('showIdsToggle');
let dictionarySelect = document.getElementById('dictionarySelect');
let retriggerButton = document.getElementById('retriggerButton');
let saveButton = document.getElementById('saveButton');
let resetButton = document.getElementById('resetButton');
let controls = document.getElementById('controls');
let hamburger = document.getElementById('hamburger');
let context = canvas.getContext('2d');
let markerIds = document.getElementById('markerIds');
let currentStream = null;
let videoDevices = [];
let currentTrack = null;
let detector = new AR.Detector();
let detecting = true;
let showIds = false;
let cameraDeviceId = null;

// Populate dictionary selection menu
function populateDictionaryMenu() {
    const dictionaries = Object.keys(AR.DICTIONARIES);
    dictionarySelect.innerHTML = '';
    dictionaries.forEach(dictionaryName => {
        let option = document.createElement('option');
        option.value = dictionaryName;
        option.text = dictionaryName;
        dictionarySelect.appendChild(option);
    });
}

// Toggle controls visibility
hamburger.addEventListener('click', () => {
    controls.style.display = controls.style.display === 'none' ? 'block' : 'none';
});

// Get the list of video input devices
async function getVideoDevices() {
    let devices = await navigator.mediaDevices.enumerateDevices();
    videoDevices = devices.filter(device => device.kind === 'videoinput');
    populateCameraMenu(videoDevices);
}

// Populate the camera selection menu
function populateCameraMenu(devices) {
    cameraSelect.innerHTML = '';
    devices.forEach((device, index) => {
        let option = document.createElement('option');
        option.value = device.deviceId;
        option.text = device.label || `Camera ${index + 1}`;
        cameraSelect.appendChild(option);
    });
}

// Start the video stream
async function startStream(deviceId) {
    if (currentStream) {
        currentStream.getTracks().forEach(track => track.stop());
    }
    let constraints = {
        video: {
            deviceId: deviceId ? { exact: deviceId } : undefined
        }
    };
    currentStream = await navigator.mediaDevices.getUserMedia(constraints);
    videoElement.srcObject = currentStream;
    currentTrack = currentStream.getVideoTracks()[0];
    setupZoomControl(currentTrack);
    requestAnimationFrame(captureFrame);
}

// Handle camera selection change
cameraSelect.addEventListener('change', async () => {
    await startStream(cameraSelect.value);
});

// Re-trigger getUserMedia
async function reinitializeCamera() {
    await getVideoDevices();
    if (videoDevices.length > 0) {
        if (cameraDeviceId) {
            cameraSelect.value = cameraDeviceId;
            await startStream(cameraDeviceId);
        } else {
            cameraSelect.value = videoDevices[0].deviceId;
            await startStream(videoDevices[0].deviceId);
        }
    } else {
        alert('No video devices found');
    }
}

// Setup zoom control
function setupZoomControl(track) {
    if ('getCapabilities' in track) {
        const capabilities = track.getCapabilities();
        if ('zoom' in capabilities) {
            zoomControl.min = capabilities.zoom.min;
            zoomControl.max = capabilities.zoom.max;
            zoomControl.step = capabilities.zoom.step;
            zoomControl.value = track.getSettings().zoom || capabilities.zoom.min;
            zoomControl.disabled = false;
        } else {
            zoomControl.disabled = true;
        }
    } else {
        zoomControl.disabled = true;
    }
}

// Apply zoom based on slider value
zoomControl.addEventListener('input', () => {
    if (currentTrack) {
        currentTrack.applyConstraints({ advanced: [{ zoom: zoomControl.value }] });
    }
});

// Toggle marker detection
detectionToggle.addEventListener('change', () => {
    detecting = detectionToggle.checked;
});

// Toggle show marker IDs
showIdsToggle.addEventListener('change', () => {
    showIds = showIdsToggle.checked;
    markerIds.style.display = showIds ? 'block' : 'none';
});

// Handle dictionary selection change
dictionarySelect.addEventListener('change', () => {
    detector = new AR.Detector({ dictionaryName: dictionarySelect.value });
});

// Initialize the interface
async function init() {
    try {
        await getVideoDevices();
        populateDictionaryMenu();
        loadSettings();
        await startStream(cameraDeviceId || videoDevices[0].deviceId);
        showIdsToggle.checked = showIds;  // Ensure the showIdsToggle is in sync with the loaded settings
        markerIds.style.display = showIds ? 'block' : 'none';
        // Ensure the GUI is refreshed after the initial setup
        reinitializeCamera();
    } catch (err) {
        console.error('Error accessing media devices.', err);
    }
    retriggerButton.addEventListener('click', reinitializeCamera);
}

// Capture frame and detect markers
function captureFrame() {
    if (videoElement.readyState === videoElement.HAVE_ENOUGH_DATA) {
        canvas.width = videoElement.videoWidth;
        canvas.height = videoElement.videoHeight;
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
        let imageData = context.getImageData(0, 0, canvas.width, canvas.height);
        if (detecting) {
            let markers = detector.detect(imageData);
            drawMarkers(markers);
        }
    }
    requestAnimationFrame(captureFrame);
}

// Draw detected markers
function drawMarkers(markers) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    markerIds.innerHTML = '';
    context.lineWidth = 3;
    let markerList = [];
    for (let i = 0; i < markers.length; i++) {
        let corners = markers[i].corners;
        context.strokeStyle = "red";
        context.beginPath();
        for (let j = 0; j < corners.length; j++) {
            let corner = corners[j];
            context.moveTo(corner.x, corner.y);
            corner = corners[(j + 1) % corners.length];
            context.lineTo(corner.x, corner.y);
        }
        context.stroke();
        context.closePath();
        context.strokeStyle = "green";
        context.strokeRect(corners[0].x - 2, corners[0].y - 2, 4, 4);
        context.strokeStyle = "blue";
        context.lineWidth = 1;
        context.strokeText(markers[i].id, corners[0].x, corners[0].y);
        
        // Calculate normalized position
        let x = (corners[0].x + corners[1].x + corners[2].x + corners[3].x) / 4 / canvas.width;
        let y = (corners[0].y + corners[1].y + corners[2].y + corners[3].y) / 4 / canvas.height;
        
        markerList.push({ id: markers[i].id, x: x.toFixed(3), y: y.toFixed(3) });
    }
    if (showIds) {
        markerList.sort((a, b) => a.id - b.id);
        markerList.forEach(marker => {
            let idElement = document.createElement('div');
            idElement.textContent = `Marker ID: ${marker.id}, X: ${marker.x}, Y: ${marker.y}`;
            markerIds.appendChild(idElement);
        });
    }
}

// Save settings to local storage
saveButton.addEventListener('click', () => {
    saveButton.classList.add('active');
    const settings = {
        camera: cameraSelect.value,
        zoom: zoomControl.value,
        detection: detectionToggle.checked,
        showIds: showIdsToggle.checked,
        dictionary: dictionarySelect.value
    };
    setTimeout(() => {
        localStorage.setItem('arucoSettings', JSON.stringify(settings));
        saveButton.classList.remove('active');
    }, 300);
});

// Load settings from local storage
function loadSettings() {
    const settings = JSON.parse(localStorage.getItem('arucoSettings'));
    if (settings) {
        cameraDeviceId = settings.camera;
        cameraSelect.value = settings.camera;
        zoomControl.value = settings.zoom;
        detectionToggle.checked = settings.detection;
        showIdsToggle.checked = settings.showIds;
        dictionarySelect.value = settings.dictionary;
        // Ensure that the detector is updated with the loaded dictionary
        detector = new AR.Detector({ dictionaryName: dictionarySelect.value });
        markerIds.style.display = settings.showIds ? 'block' : 'none';
        showIds = settings.showIds;
    }
}

// Reset settings to default
resetButton.addEventListener('click', () => {
    resetButton.classList.add('active');
    setTimeout(() => {
        localStorage.removeItem('arucoSettings');
        resetButton.classList.remove('active');
        location.reload();
    }, 300);
});

// Initialize app
init();