const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const fpsDisplay = document.getElementById('fps');

let previousTime = performance.now();
let frameCount = 0;

async function startVideo() {
    try {
        const stream = await navigator.mediaDevices.getUserMedia({ video: true });
        video.srcObject = stream;
        video.onloadedmetadata = () => {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            requestAnimationFrame(detectMarkers);
        };
    } catch (error) {
        console.error('Error accessing the camera:', error);
    }
}

function detectMarkers() {
    const currentTime = performance.now();
    frameCount++;

    if (currentTime - previousTime >= 1000) {
        const fps = (frameCount / ((currentTime - previousTime) / 1000)).toFixed(1);
        fpsDisplay.textContent = `FPS: ${fps}`;
        previousTime = currentTime;
        frameCount = 0;
    }

    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

    // Initialize AR.js context for ARUCO marker detection
    const arToolkitContext = new THREEx.ArToolkitContext({
        detectionMode: 'mono',
        canvasWidth: canvas.width,
        canvasHeight: canvas.height,
    });

    const arToolkitSource = new THREEx.ArToolkitSource({ sourceType: 'webcam' });
    arToolkitSource.init(() => {
        onResize();
    });

    window.addEventListener('resize', onResize);
    function onResize() {
        arToolkitSource.onResize();
        arToolkitSource.copySizeTo(canvas);
        if (arToolkitContext.arController !== null) {
            arToolkitSource.copySizeTo(arToolkitContext.arController.canvas);
        }
    }

    // Detect ARUCO markers
    arToolkitContext.init(() => {
        const arController = arToolkitContext.arController;
        arController.addEventListener('getMarker', (event) => {
            console.log('Marker detected:', event);
        });
    });

    arToolkitContext.update(arToolkitSource.domElement);

    requestAnimationFrame(detectMarkers);
}

startVideo();
