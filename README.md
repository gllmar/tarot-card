# tarot-card-tracker

## database stucture

### Video Background Standby

* [Video Background Standby](./player/keytagVideoPlayerBackground/index.html)

### ram loader

* [ram_loader player DEMO](./player/mdjson_toRam/index.html)
* [ram_loader readme](./player/mdjson_toRam/readme.md)
* [ram_loader database](./player/medias/lorem/database.md)


### mdjson

* [mdjson_loader player DEMO](./player/mdjson_loader/index.html)
* [mdjson_loader readme](./player/mdjson_loader/readme.md)
* [mdjson_loader database](./player/mdjson_loader/database.md)

## Tracking technologie

### RFID
* [RFID tag video player DEMO](./player/keytagVideoPlayer/index.html)
* [RFID tag video player](./player/keytagVideoPlayer/readme.md)

* ![video](./player/keytagVideoPlayer/docs/keytag_player_demo.mp4)



### Aruco
#### Javascript web -> js-aruco2 

[Mobile friendly implementation](./tracker/aruco.js/arucojs-tracker/index.html)

### Card aruco marker tracker 

* [Card PDF](template/design-letter.pdf) 

* [card SVG](template/design-letter.svg) 

<!-- #### OpenCV Native
* https://docs.opencv.org/4.x/d5/dae/tutorial_aruco_detection.html -->

<!-- 
#### OpenCV js
* Via wasm  -->


##### Base on js-aruco2

* [https://github.com/damianofalcioni/js-aruco2](https://github.com/damianofalcioni/js-aruco2)

* [Demo live](https://damianofalcioni.github.io/js-aruco2/samples/getusermedia/getusermedia_ARUCO.html)

* ![video](media/2024-06-06_12-55-08.mp4)




## 78 cards

### 22 Major Arcana 


1. The Fool
    * Beginnings, innocence, spontaneity, a free spirit
2. The Magician
    * Manifestation, resourcefulness, power, inspired action
3. The High Priestess
    * Intuition, sacred knowledge, divine feminine, the subconscious mind
4. The Empress
    * Femininity, beauty, nature, nurturing, abundance
5. The Emperor
    * Authority, establishment, structure, a father figure
6. The Hierophant
    * Spiritual wisdom, religious beliefs, conformity, tradition, institutions
7. The Lovers
    * Love, harmony, relationships, values alignment, choices
8. The Chariot
    * Control, willpower, success, action, determination
9. Strength
    * Courage, persuasion, influence, compassion
10. The Hermit
    * Soul-searching, introspection, being alone, inner guidance
11. Wheel of Fortune
    * Good luck, karma, life cycles, destiny, a turning point
12. Justice
    * Justice, fairness, truth, cause and effect, law
13. The Hanged Man
    * Pause, surrender, letting go, new perspectives
14. Death
    * Endings, change, transformation, transition
15. Temperance
    * Balance, moderation, patience, purpose
16. The Devil
    * Shadow self, attachment, addiction, restriction, sexuality
17. The Tower
    * Sudden change, upheaval, chaos, revelation, awakening
18. The Star
    * Hope, faith, purpose, renewal, spirituality
19. The Moon
    * Illusion, fear, anxiety, subconscious, intuition
20. The Sun
    * Positivity, fun, warmth, success, vitality
21. Judgement
    * Judgement, rebirth, inner calling, absolution
22. The World
    * Completion, integration, accomplishment, travel

### 56 minor Arcana 

#### Cups 
Associated with emotions and relationships

1. Ace of Cups
    * Love, new relationships, compassion, creativity
2. Two of Cups
    * Unified love, partnership, mutual attraction
3. Three of Cups
    * Celebration, friendship, creativity, collaborations
4. Four of Cups
    * Meditation, contemplation, apathy, re-evaluation
5. Five of Cups
    * Regret, failure, disappointment, pessimism
6. Six of Cups
    * Revisiting the past, childhood memories, innocence, joy
7. Seven of Cups
    * Opportunities, choices, wishful thinking, illusion
8. Eight of Cups
    * Disappointment, abandonment, withdrawal, escapism
9. Nine of Cups
    * Contentment, satisfaction, gratitude, wish come true
10. Ten of Cups
    * Divine love, blissful relationships, harmony, alignment
11. Page of Cups
    * Creative opportunities, intuitive messages, curiosity, possibility
12. Knight of Cups
    * Romance, charm, imagination, beauty
13. Queen of Cups
    * Compassion, care, emotional stability, intuition
14. King of Cups
    * Emotional balance and control, generosity, compassion

#### Pentacles 
Associated with material aspects, career, and finance

1. Ace of Pentacles
    * A new financial or career opportunity, manifestation, abundance
2. Two of Pentacles
    * Multiple priorities, time management, adaptability
3. Three of Pentacles
    * Teamwork, collaboration, learning, implementation
4. Four of Pentacles
    * Saving money, security, conservatism, scarcity, control
5. Five of Pentacles
    * Financial loss, poverty, lack mindset, isolation, worry
6. Six of Pentacles
    * Giving, receiving, sharing wealth, generosity, charity
7. Seven of Pentacles
    * Long-term view, sustainable results, perseverance, investment
8. Eight of Pentacles
    * Apprenticeship, repetitive tasks, mastery, skill development
9. Nine of Pentacles
    * Abundance, luxury, self-sufficiency, financial independence
10. Ten of Pentacles
    * Wealth, financial security, family, long-term success, contribution
11. Page of Pentacles
    * Manifestation, financial opportunity, skill development
12. Knight of Pentacles
    * Hard work, productivity, routine, conservatism
13. Queen of Pentacles
    * Nurturing, practical, providing financially, a working parent
14. King of Pentacles
    * Wealth, business, leadership, security, discipline, abundance

#### Swords 
associated with intellect, thoughts, and conflict

1. Ace of Swords - Breakthrough, clarity, sharp mind
2. Two of Swords - Difficult choices, indecision, stalemate, truce
3. Three of Swords - Heartbreak, emotional pain, sorrow, grief, hurt
4. Four of Swords - Rest, relaxation, meditation, contemplation, recuperation
5. Five of Swords - Conflict, disagreements, competition, defeat, winning at all costs
6. Six of Swords - Transition, change, rite of passage, releasing baggage
7. Seven of Swords - Betrayal, deception, getting away with something, acting strategically
8. Eight of Swords - Negative thoughts, self-imposed restriction, imprisonment, victim mentality
9. Nine of Swords - Anxiety, worry, fear, depression, nightmares
10. Ten of Swords - Painful endings, deep wounds, betrayal, loss, crisis
11. Page of Swords - New ideas, curiosity, thirst for knowledge, new ways of communicating
12. Knight of Swords - Ambitious, action-oriented, driven to succeed, fast-thinking
13. Queen of Swords - Independent, unbiased judgment, clear boundaries, direct communication
14. King of Swords - Mental clarity, intellectual power, authority, truth

#### Wands 
associated with inspiration, spirituality, determination

1. Ace of Wands
    * Inspiration, new opportunities, growth, potential
2. Two of Wands
    * Future planning, progress, decisions, discovery
3. Three of Wands
    * Expansion, foresight, overseas opportunities
4. Four of Wands
    * Celebration, joy, harmony, relaxation, homecoming
5. Five of Wands
    * Conflict, disagreements, competition, tension, diversity
6. Six of Wands
    * Success, public recognition, progress, self-confidence
7. Seven of Wands
    * Challenge, competition, protection, perseverance
8. Eight of Wands
    * Movement, fast-paced change, action, alignment, air travel
9. Nine of Wands
    * Resilience, courage, persistence, test of faith, boundaries
10. Ten of Wands
    * Burden, extra responsibility, hard work, completion
11. Page of Wands
    * Inspiration, ideas, discovery, limitless potential, free spirit
12. Knight of Wands
    * Energy, passion, inspired action, adventure, impulsiveness
13. Queen of Wands
    * Courage, confidence, independence, social butterfly, determination
14. King of Wands
    * Natural-born leader, vision, entrepreneur, honor





U+1F0E0 🃠 Fool
U+1F0E1 🃡 Trump-1 (individual)
U+1F0E2 🃢 Trump-2 (childhood)
U+1F0E3 🃣 Trump-3 (youth)
U+1F0E4 🃤 Trump-4 (maturity)
U+1F0E5 🃥 Trump-5 (old age)
U+1F0E6 🃦 Trump-6 (morning)
U+1F0E7 🃧 Trump-7 (afternoon)
U+1F0E8 🃨 Trump-8 (evening)
U+1F0E9 🃩 Trump-9 (night)
U+1F0EA 🃪 Trump-10 (earth and air)
U+1F0EB 🃫 Trump-11 (water and fire)
U+1F0EC 🃬 Trump-12 (dance)
U+1F0ED 🃭 Trump-13 (shopping)
U+1F0EE 🃮 Trump-14 (open air)
U+1F0EF 🃯 Trump-15 (visual arts)
U+1F0F0 🃰 Trump-16 (spring)
U+1F0F1 🃱 Trump-17 (summer)
U+1F0F2 🃲 Trump-18 (autumn)
U+1F0F3 🃳 Trump-19 (winter)
U+1F0F4 🃴 Trump-20 (the game)
U+1F0F5 🃵 Trump-21 (collective)


```

U+1F0E0 	U+1F0E1 	U+1F0E2 	U+1F0E3
🃠 	🃡 	🃢 	🃣
Fool 	Trump-1 (individual) 	Trump-2 (childhood) 	Trump-3 (youth)
U+1F0E4 	U+1F0E5 	U+1F0E6 	U+1F0E7
🃤 	🃥 	🃦 	🃧
Trump-4 (maturity) 	Trump-5 (old age) 	Trump-6 (morning) 	Trump-7 (afternoon)
U+1F0E8 	U+1F0E9 	U+1F0EA 	U+1F0EB
🃨 	🃩 	🃪 	🃫
Trump-8 (evening) 	Trump-9 (night) 	Trump-10 (earth and air) 	Trump-11 (water and fire)
U+1F0EC 	U+1F0ED 	U+1F0EE 	U+1F0EF
🃬 	🃭 	🃮 	🃯
Trump-12 (dance) 	Trump-13 (shopping) 	Trump-14 (open air) 	Trump-15 (visual arts)
U+1F0F0 	U+1F0F1 	U+1F0F2 	U+1F0F3
🃰 	🃱 	🃲 	🃳
Trump-16 (spring) 	Trump-17 (summer) 	Trump-18 (autumn) 	Trump-19 (winter)
U+1F0F4 	U+1F0F5 		
🃴 	🃵 		
Trump-20 (the game) 	Trump-21 (collective)
```


Playing Cards[1][2]
Official Unicode Consortium code chart (PDF)
```
            0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	A 	B 	C 	D 	E 	F
U+1F0Ax 	🂠 	🂡 	🂢 	🂣 	🂤 	🂥 	🂦 	🂧 	🂨 	🂩 	🂪 	🂫 	🂬 	🂭 	🂮 	
U+1F0Bx 		🂱 	🂲 	🂳 	🂴 	🂵 	🂶 	🂷 	🂸 	🂹 	🂺 	🂻 	🂼 	🂽 	🂾 	🂿
U+1F0Cx 		🃁 	🃂 	🃃 	🃄 	🃅 	🃆 	🃇 	🃈 	🃉 	🃊 	🃋 	🃌 	🃍 	🃎 	🃏
U+1F0Dx 		🃑 	🃒 	🃓 	🃔 	🃕 	🃖 	🃗 	🃘 	🃙 	🃚 	🃛 	🃜 	🃝 	🃞 	🃟
U+1F0Ex 	🃠 	🃡 	🃢 	🃣 	🃤 	🃥 	🃦 	🃧 	🃨 	🃩 	🃪 	🃫 	🃬 	🃭 	🃮 	🃯
U+1F0Fx 	🃰 	🃱 	🃲 	🃳 	🃴 	🃵 										
Notes
```

1.^ As of Unicode version 15.1
2.^ Grey areas indicate non-assigned code points
