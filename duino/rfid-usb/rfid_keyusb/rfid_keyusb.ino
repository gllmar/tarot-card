#include <Wire.h>
#include "MFRC522_I2C.h"
#include <M5AtomS3.h>
#include <FastLED.h>
#include "USB.h"
#include "USBHIDKeyboard.h"

// Pin definitions
const int buttonPin = 41; // Button pin for M5AtomS3Lite
const int rgbPin = 35; // RGB LED pin for M5AtomS3Lite

// I2C GPIOs
// #define IIC_CLK 1
// #define IIC_DATA 2

byte chipAddress = 0x28;

TwoWire i2cBus = TwoWire(0);
MFRC522 mfrc522(chipAddress);  // Initialize with I2C address only

// FastLED setup
#define NUM_LEDS 1
CRGB leds[NUM_LEDS];
USBHIDKeyboard Keyboard;
int previousButtonState = HIGH;  // for checking the state of a pushButton

void setup() {
  // Initialize M5AtomS3
  M5.begin();
  FastLED.addLeds<WS2812, rgbPin, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(100);

  // Set initial LED color
  leds[0] = CRGB::Red;
  FastLED.show();

  // Make the pushButton pin an input
  pinMode(buttonPin, INPUT_PULLUP);

  // Initialize control over the keyboard
  Keyboard.begin();
  USB.begin();

  // Seed the random number generator
  randomSeed(analogRead(0));

  // Set initial standby LED color
  leds[0] = CRGB::Blue;
  FastLED.show();

  // Initialize I2C bus and MFRC522
  Wire.begin();
  mfrc522.PCD_Init();

  // Set to standby color after initialization
  leds[0] = CRGB::Blue;
  FastLED.show();
}

void loop() {
  // Read the pushbutton
  int buttonState = digitalRead(buttonPin);

  // Look for new cards
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {
    // Print the UID of the card
    Serial.print("UID:");
    String uidString = "";
    for (byte i = 0; i < mfrc522.uid.size; i++) {
      if (mfrc522.uid.uidByte[i] < 0x10) {
        uidString += "0";
      }
      uidString += String(mfrc522.uid.uidByte[i], HEX);
      Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      Serial.print(mfrc522.uid.uidByte[i], HEX);
    }
    Serial.println();

    // Send UID as keyboard input
    Keyboard.print(uidString);
    Keyboard.println();

    // Change LED color to indicate a successful read
    leds[0] = CRGB::Green;
    FastLED.show();
    delay(500); // Hold the color for a moment

    // Return to standby color
    leds[0] = CRGB::Blue;
    FastLED.show();

    // Halt PICC
    mfrc522.PICC_HaltA();
  }

  // Save the current button state for comparison next time
  previousButtonState = buttonState;
}
