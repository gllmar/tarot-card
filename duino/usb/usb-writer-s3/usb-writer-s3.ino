#include <M5AtomS3.h>
#include <FastLED.h>
#include "USB.h"
#include "USBHIDKeyboard.h"

// Pin definitions
const int buttonPin = 41; // Button pin for M5AtomS3Lite
const int rgbPin = 35; // RGB LED pin for M5AtomS3Lite

// FastLED setup
#define NUM_LEDS 1
CRGB leds[NUM_LEDS];
USBHIDKeyboard Keyboard;
int previousButtonState = HIGH;  // for checking the state of a pushButton

void setup() {
  // Initialize M5AtomS3
  M5.begin();
  FastLED.addLeds<WS2812, rgbPin, GRB>(leds, NUM_LEDS);
  FastLED.setBrightness(100);

  // Set initial LED color
  leds[0] = CRGB::Red;
  FastLED.show();

  // Make the pushButton pin an input
  pinMode(buttonPin, INPUT_PULLUP);

  // Initialize control over the keyboard
  Keyboard.begin();
  USB.begin();

  // Seed the random number generator
  randomSeed(analogRead(0));

  // Set initial standby LED color
  leds[0] = CRGB::Blue;
  FastLED.show();
}

void loop() {
  // Read the pushbutton
  int buttonState = digitalRead(buttonPin);

  // If the button state has changed and it's currently pressed
  if ((buttonState != previousButtonState) && (buttonState == LOW)) {
    // Generate a random letter (A-Z)
    char randomLetter = 'A' + random(26);

    // Print the random letter
    Keyboard.print(randomLetter);

    // Change LED color to indicate button press
    uint32_t colors[] = {CRGB::Red, CRGB::Green, CRGB::Blue, CRGB::Yellow, CRGB::Magenta, CRGB::Cyan};
    static int colorIndex = 0;
    leds[0] = colors[colorIndex];
    FastLED.show();
    colorIndex = (colorIndex + 1) % 6;
  }

  // Save the current button state for comparison next time
  previousButtonState = buttonState;
}