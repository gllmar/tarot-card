#include <Wire.h>
#include "MFRC522_I2C.h"
#include <M5AtomS3.h>

byte chipAddress = 0x28;

TwoWire i2cBus = TwoWire(0);
MFRC522 mfrc522(chipAddress);  // Initialize with I2C address only

void setup() {
  // Initialize M5AtomS3
  AtomS3.begin(true);  // Init M5AtomS3Lite
  AtomS3.dis.setBrightness(100);

  // Set initial standby LED color
  AtomS3.dis.drawpix(0xff0000);  // Red color for booting
  AtomS3.update();

  Serial.begin(115200);           // Initialize serial communications with the PC
  Wire.begin();  // Wire init, adding the I2C bus.

  Serial.println("Initializing I2C Bus");
  Serial.println("Initializing MFRC522");
  mfrc522.PCD_Init();
  Serial.println("MFRC522 Selftest");

  // Set to standby color after initialization
  AtomS3.dis.drawpix(0x0000ff);  // Blue color for standby
  AtomS3.update();
}

void loop() {
  // Look for new cards
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  // Print the UID of the card
  Serial.print("UID:");
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  Serial.println();

  // Change LED color to indicate success
  AtomS3.dis.drawpix(0x00ff00);  // Green color for success
  AtomS3.update();
  delay(200);                    // Wait for 200 milliseconds
  AtomS3.dis.drawpix(0x0000ff);  // Back to standby blue color
  AtomS3.update();

  // Halt PICC
  mfrc522.PICC_HaltA();
  delay(200);
}

void ShowReaderDetails() {
  byte version = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);

  switch (version) {
    case 0x88:  // Fudan Semiconductor FM17522 clone
      Serial.println("Fudan Semiconductor FM17522 clone");
      break;
    case 0x90:  // Version 0.0
      Serial.println("Version 0.0");
      break;
    case 0x91:  // Version 1.0
      Serial.println("Version 1.0");
      break;
    case 0x92:  // Version 2.0
      Serial.println("Version 2.0");
      break;
    default:  // Unknown version
      Serial.println("Unknown version");
  }
}
