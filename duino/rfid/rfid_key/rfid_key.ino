#include <Wire.h>
#include "MFRC522_I2C.h"
#include <M5AtomS3.h>
#include <Adafruit_TinyUSB.h>  // Include the TinyUSB library

// I2C GPIOs
#define IIC_CLK 1
#define IIC_DATA 2

#define RST_PIN 22

byte chipAddress = 0x28;

TwoWire i2cBus = TwoWire(0);
MFRC522 mfrc522(chipAddress);  // Initialize with I2C address only

// HID report descriptor using TinyUSB's template
uint8_t const desc_hid_report[] = {
    TUD_HID_REPORT_DESC_KEYBOARD()
};

// USB HID object
Adafruit_USBD_HID usb_hid(desc_hid_report, sizeof(desc_hid_report), HID_ITF_PROTOCOL_KEYBOARD, 2, false);

void setup() {
  // Initialize M5AtomS3
  AtomS3.begin(true);  // Init M5AtomS3Lite
  AtomS3.dis.setBrightness(100);

  // Set initial standby LED color
  AtomS3.dis.drawpix(0xff0000);  // Red color for booting
  AtomS3.update();

  Serial.begin(115200);           // Initialize serial communications with the PC
  Wire.begin(IIC_DATA, IIC_CLK, 40000);  // Initialize I2C

  Serial.println("Initializing I2C Bus");
  Serial.println("Initializing MFRC522");
  mfrc522.PCD_Init();
  Serial.println("MFRC522 Selftest");

  // Set to standby color after initialization
  AtomS3.dis.drawpix(0x0000ff);  // Blue color for standby
  AtomS3.update();

  // Initialize TinyUSB and HID
  usb_hid.begin();
  usb_hid.setPollInterval(2);
}

void loop() {
  #ifdef TINYUSB_NEED_POLLING_TASK
  TinyUSBDevice.task();
  #endif

  // Look for new cards
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  // Print the UID of the card to Serial
  Serial.print("UID:");
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  Serial.println();

  // Send UID as keyboard input
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    char key = mfrc522.uid.uidByte[i];
    usb_hid.keyboardPress(0, key);
    delay(50);
    usb_hid.keyboardRelease(0);
  }
  usb_hid.keyboardPress(0, HID_KEY_RETURN);
  delay(50);
  usb_hid.keyboardRelease(0);

  // Change LED color to indicate success
  AtomS3.dis.drawpix(0x00ff00);  // Green color for success
  AtomS3.update();
  delay(200);                    // Wait for 200 milliseconds
  AtomS3.dis.drawpix(0x0000ff);  // Back to standby blue color
  AtomS3.update();

  // Halt PICC
  mfrc522.PICC_HaltA();
  delay(200);
}

void ShowReaderDetails() {
  byte version = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);

  switch (version) {
    case 0x88:  // Fudan Semiconductor FM17522 clone
      Serial.println("Fudan Semiconductor FM17522 clone");
      break;
    case 0x90:  // Version 0.0
      Serial.println("Version 0.0");
      break;
    case 0x91:  // Version 1.0
      Serial.println("Version 1.0");
      break;
    case 0x92:  // Version 2.0
      Serial.println("Version 2.0");
      break;
    default:  // Unknown version
      Serial.println("Unknown version");
  }
}
