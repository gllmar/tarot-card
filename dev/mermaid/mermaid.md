```mermaid
sequenceDiagram
    participant Peer1
    participant SignalingServer
    participant Peer2
    
    Note over Peer1: Initialization
    Peer1->>SignalingServer: Connect
    SignalingServer-->>Peer1: Welcome message
    
    Note over Peer2: Initialization
    Peer2->>SignalingServer: Connect
    SignalingServer-->>Peer2: Welcome message
    
    Note over Peer1: Create and send offer
    Peer1->>SignalingServer: Send offer (SDP)
    SignalingServer-->>Peer2: Relay offer (SDP)
    
    Note over Peer2: Handle offer and create answer
    Peer2->>SignalingServer: Send answer (SDP)
    SignalingServer-->>Peer1: Relay answer (SDP)
    
    Note over Peer1, Peer2: Exchange ICE candidates
    Peer1->>SignalingServer: Send ICE candidate
    SignalingServer-->>Peer2: Relay ICE candidate
    
    Peer2->>SignalingServer: Send ICE candidate
    SignalingServer-->>Peer1: Relay ICE candidate
```